﻿
using System.Windows.Forms;

namespace Leeson_1.Window
{
    class MainTabControl: TabControl
    {

        public MainTabPage.SumTabPage _sumTabPage = new MainTabPage.SumTabPage();
                
        public void Initialized()
        {
            this.Size = new System.Drawing.Size(600, 600);

            _sumTabPage.Initialized();
            this.Controls.Add(_sumTabPage);


        }

    }
}
