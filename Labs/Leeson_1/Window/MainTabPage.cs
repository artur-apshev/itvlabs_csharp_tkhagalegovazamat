﻿namespace Leeson_1.Window
{
    class MainTabPage
    {

        public static void CloseWindow(object sender, System.EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        public static void EnterOnlyDigits(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            e.Handled = !(char.IsDigit(c)
                          || c == '.'
                          || c == ','
                          || c == 8);// we can enter number and dot
        }

        public static void EnterOnlyDigitsWithoutChar(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            e.Handled = !(char.IsDigit(c)
                          || c == 8
                          || c == 45);// we can enter number
        }

        public class SumTabPage : System.Windows.Forms.TabPage
        {
            public void SumThreeNumber(object sender, System.EventArgs e)
            {
                int number = Algebra.calculationSum(System.Convert.ToDouble(_InputTextBox.Text));
                _outPutResultInfo.Text = number.ToString();
            }
            public void Initialized()
            {
                _buttonOK = new System.Windows.Forms.Button();
                _buttonExit = new System.Windows.Forms.Button();
                _InputTextBox = new System.Windows.Forms.TextBox();
                _outPutResultInfo = new System.Windows.Forms.Label();
                _headInfoLabel = new System.Windows.Forms.Label();
                //Button OK
                _buttonOK.Text = Helper.Constants.BUTTON_OK_TXT;
                _buttonOK.Size = new System.Drawing.Size(100, 50);
                _buttonOK.Location = new System.Drawing.Point(100, 400);
                _buttonOK.BackColor = System.Drawing.Color.White;
                _buttonOK.Font = new System.Drawing.Font("Aray", 14);
                _buttonOK.Click += SumThreeNumber;

                //Button Exit
                _buttonExit.Text = Helper.Constants.BUTTON_EXIT_TXT;
                _buttonExit.Size = new System.Drawing.Size(100, 50);
                _buttonExit.Location = new System.Drawing.Point(350, 400);
                _buttonExit.BackColor = System.Drawing.Color.White;
                _buttonExit.Font = new System.Drawing.Font("Aray", 14);
                _buttonExit.Click += CloseWindow;

                // _InputTextBox
                _InputTextBox.Size = new System.Drawing.Size(400, 100);
                _InputTextBox.Location = new System.Drawing.Point(100, 100);
                _InputTextBox.Font = new System.Drawing.Font("Aray", 32);
                _InputTextBox.KeyPress += EnterOnlyDigits;
                // _outPutResultInfo
                _outPutResultInfo.Text = "C#";
                _outPutResultInfo.Size = new System.Drawing.Size(400, 300);
                _outPutResultInfo.Location = new System.Drawing.Point(100, 200);
                _outPutResultInfo.Font = new System.Drawing.Font("Georgia", 72);
                //_headInfoLabel
                _headInfoLabel.Text = Helper.Constants.INPUT_SUM_HEAD_INFO;
                _headInfoLabel.Size = new System.Drawing.Size(400, 300);
                _headInfoLabel.Location = new System.Drawing.Point(100, 50);
                _headInfoLabel.Font = new System.Drawing.Font("Georgia", 18);
                ///

                this.BackColor = System.Drawing.Color.DarkRed;
                this.Text = "Sum";
                this.Font = new System.Drawing.Font("Aray", 11);
                this.Controls.Add(_buttonOK);
                this.Controls.Add(_buttonExit);
                this.Controls.Add(_InputTextBox);
                this.Controls.Add(_outPutResultInfo);
                this.Controls.Add(_headInfoLabel);
            }
            private static System.Windows.Forms.Button _buttonOK;
            private static System.Windows.Forms.Button _buttonExit;
            private static System.Windows.Forms.TextBox _InputTextBox;
            private static System.Windows.Forms.Label _outPutResultInfo;
            public static System.Windows.Forms.Label _headInfoLabel;

        }

    }
}
