﻿using Leeson_1.Helper;
using System;
using System.Drawing;

using System.Windows.Forms;

namespace Leeson_1.Window
{
    class MainWindow : Form
    {
        public void ShowWindow()
        {

            Application.Run(this);
        }

        public MainWindow configurationWindow()
        {
             _mainTabControl = new MainTabControl();
            _mainTabControl.Initialized();
            this.Controls.Add(_mainTabControl);
            this.Size = new Size(600, 600);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            return this;
        }

        private static MainTabControl _mainTabControl;
    }
 }
