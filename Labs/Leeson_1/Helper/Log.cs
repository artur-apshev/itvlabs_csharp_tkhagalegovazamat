﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leeson_1.Helper;

namespace Leeson_1.Helper
{
    class Log
    {
        private static Logger _logger = new Logger();

        internal void _Log(string message)
        {
            File.AppendAllText(Constants.LOG_OUTPUT_FILE,
                String.Format("\n[{0}]: [{1}]  [{2}]--{3}", Constants.DEBUG, DateTime.Now.Date, DateTime.Now.TimeOfDay, message));
        }


        public static void DEBUG(string format, params object[] args)
        {
            _logger.Debug(string.Format(format, args));
        }

        public static void DEBUG(Exception e, string format, params object[] args)
        {
            _logger.Debug(e, string.Format(format, args));
        }


        public static void INFO(string format, params object[] args)
        {
            _logger.Info(string.Format(format, args));
        }


        public static void Info(Exception e, string format, params object[] args)
        {
            _logger.Info(e, string.Format(format, args));
        }


        public static void WARN(string format, params object[] args)
        {
            _logger.Warn(string.Format(format, args));
        }


        public static void WARN(Exception e, string format, params object[] args)
        {
            _logger.Warn(e, string.Format(format, args));
        }


        public static void ERROR(string format, params object[] args)
        {
            _logger.Error(string.Format(format, args));
        }


        public static void ERROR(Exception e, string format, params object[] args)
        {
            _logger.Error(e, string.Format(format, args));
        }

        public static void FATAL(Exception e, string format, params object[] args)
        {
            _logger.Fatal(e, string.Format(format, args));
        }

        public static void FATAL(string format, params object[] args)
        {
            _logger.Fatal(string.Format(format, args));
        }
    }
    public interface ILogger
    {

        void Debug(string logEntry);

        void Debug(Exception t, string logEntry);

        void Info(string logEntry);

        void Info(Exception t, string logEntry);

        void Warn(string logEntry);

        void Warn(Exception t, string logEntry);

        void Error(string logEntry);

        void Error(Exception t, string logEntry);

        void Fatal(string logEntry);

        void Fatal(Exception t, string logEntry);

    }
}
