﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leeson_1.Helper;

namespace Leeson_1.Helper
{
    public class Logger: ILogger
    {
        private void outPutLogger(string message, string loggerType)
        {
            File.AppendAllText(Constants.LOG_OUTPUT_FILE,
                String.Format("\n[{0}]: [{1}]  [{2}]--{3}", loggerType, DateTime.Now.Date, DateTime.Now.TimeOfDay, message));
        }

        public void Debug(string logEntry)
        {
            outPutLogger(logEntry, Constants.DEBUG);

        }

        public void Debug(Exception t, string logEntry)
        {
            outPutLogger(string.Format("\n{0}: {1}", t, logEntry), Constants.DEBUG);
        }

        public void Info(string logEntry)
        {
            outPutLogger(logEntry, Constants.INFO);
        }

        public void Info(Exception t, string logEntry)
        {
        }

        public void Warn(string logEntry)
        {
            outPutLogger(string.Format("\n{0}", logEntry), Constants.WARN);
        }

        public void Warn(Exception t, string logEntry)
        {
            outPutLogger(string.Format("\n{0}: {1}", t, logEntry), Constants.WARN);
        }


        public void Error(string logEntry)
        {
            outPutLogger(logEntry, Constants.ERROR);
        }


        public void Error(Exception t, string logEntry)
        {
            outPutLogger(string.Format("\n{0}: {1}", t, logEntry), Constants.ERROR);
        }


        public void Fatal(string logEntry)
        {
            outPutLogger(logEntry, Constants.FATAL);
        }


        public void Fatal(Exception t, string logEntry)
        {
            outPutLogger(string.Format("\n{0}: {1}", t, logEntry), Constants.FATAL);
        }
    }
}
